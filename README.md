# README #

### What is this repository for? ###

Simple C# Application which displays the call log of your AVM FRITZ!Box. It is able to fetch contacts from Outlook and Google Mail. Outlook.com/Live Contacts Support is currently in development.

### How do I get set up? ###

- MSBUILD / Visual Studio (2015)
- nuget cares about all dependencies
- Configuration happens about typical .NET Settings XML