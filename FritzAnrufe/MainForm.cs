﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using FritzAnrufe.AddressBook;
using FritzAnrufe.Device;
using FritzAnrufe.Properties;

namespace FritzAnrufe
{
    //TODO: Adressquellen
    //TODO: Anruflistenquellen
    //TODO: WPF ... Ribbon + Glass ??
    //TODO: csharp libphonenummer sinnvoll?

    public partial class MainForm : Form
    {
        public readonly Dictionary<string, string> AddressBook;

        public bool FinalClose;

        public MainForm()
        {
            AddressBookType abtype;

            Enum.TryParse(Settings.Default.AddressBook, out abtype);

            switch (abtype)
            {
                case AddressBookType.None:
                    AddressBook = new NullAddressBookConnector().CollectNamePhonePairs();
                    break;
                case AddressBookType.Google:
                    AddressBook =
                        new GoogleAddressBookConnector(Utility.Base64Decode(Settings.Default.GoogleUser),
                            Utility.Base64Decode(Settings.Default.GooglePw), Settings.Default.Land).
                            CollectNamePhonePairs();
                    break;
                case AddressBookType.Outlook:
//                    AddressBook = new OutlookAddressBookConnector().CollectNamePhonePairs();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            InitializeComponent();

            RefreshButtonClick(null, EventArgs.Empty);
        }

        private void RefreshButtonClick(object sender, EventArgs e)
        {
            listView.Enabled = false;
            listView.Items.Clear();
            backgroundWorker.RunWorkerAsync(new UpdateDeviceAction());
        }

        private void ToolStripButton1Click(object sender, EventArgs e)
        {
            listView.Enabled = false;
            listView.Items.Clear();
            backgroundWorker.RunWorkerAsync(new ClearDeviceAction());
        }

        private void ToolStripMenuItem1Click(object sender, EventArgs e)
        {
            Show();
            BringToFront();
        }

        private void ToolStripMenuItem2Click(object sender, EventArgs e)
        {
            FinalClose = true;
            Close();
            Application.Exit();
        }

        private void NotifyIconMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (Visible)
                    Hide();
                else
                {
                    Show();
                    BringToFront();
                }
            }
        }

        private void MainFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!FinalClose)
            {
                Hide();
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = ((IDeviceAction) e.Argument).Run(this, new FritzBoxDevice(this));
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var items = (List<ListViewItem>) e.Result;
            if (items != null)
                foreach (ListViewItem item in items)
                    listView.Items.Add(item);

            listView.Enabled = true;
            foreach (ColumnHeader header in listView.Columns)
            {
                header.Width = -2;
            }
        }
    }
}