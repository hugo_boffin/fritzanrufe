using System;
using System.Collections.Generic;
using System.Windows.Forms;
using FritzAnrufe.Device;
using FritzAnrufe.Properties;

namespace FritzAnrufe
{
    internal class UpdateDeviceAction : IDeviceAction
    {
        #region IDeviceAction Members

        public List<ListViewItem> Run(MainForm mainForm, ITelephonyDevice telephonyDevice)
        {
            List<Call> calls = telephonyDevice.GetCallList();

            // View
            var items = new List<ListViewItem>();
            foreach (Call call in calls)
            {
                var fields = new String[5];
                fields[0] = call.Date;
                fields[1] = call.Caller;
                fields[2] = call.Device;
                fields[3] = call.Called;
                fields[4] = call.Duration;

                //TODO: Vertauschung usw. umbauen
                if (call.Type == 2)
                {
                    string swap = fields[1];
                    fields[1] = fields[3];
                    fields[3] = swap;
                }

                if (fields[1].Length != 0)
                {
                    string neu = fields[1];

                    //TODO: Nachschlagen der Namen
                    if (mainForm.AddressBook.ContainsKey(fields[1]))
                        neu = mainForm.AddressBook[fields[1]];
                    else if (!fields[1].StartsWith(Settings.Default.Land) &&
                             mainForm.AddressBook.ContainsKey(Settings.Default.Vorwahl + fields[1]))
                        neu = mainForm.AddressBook[Settings.Default.Vorwahl + fields[1]];

                    fields[1] = neu;
                }
                else
                {
                    fields[1] = "Unbekannt";
                }

                items.Add(new ListViewItem(fields, call.Type));
            }

            return items;
        }

        #endregion
    }
}