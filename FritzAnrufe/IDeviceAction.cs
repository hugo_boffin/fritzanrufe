using System.Collections.Generic;
using System.Windows.Forms;
using FritzAnrufe.Device;

namespace FritzAnrufe
{
    internal interface IDeviceAction
    {
        // TODO MainForm weg, auf Observer umbauen (OnCallListUpdated oder so...)
        List<ListViewItem> Run(MainForm mainForm, ITelephonyDevice telephonyDevice);
    }
}