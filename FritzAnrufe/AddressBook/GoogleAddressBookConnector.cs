using System.Collections.Generic;
using Google.GData.Contacts;
using Google.GData.Extensions;

namespace FritzAnrufe.AddressBook
{
    internal class GoogleAddressBookConnector : IAddressBookConnector
    {
        private readonly string _land;
        private readonly string _password;
        private readonly string _username;

        public GoogleAddressBookConnector(string username, string password, string land)
        {
            _username = username;
            _password = password;
            _land = land;
        }

        #region IAddressBookConnector Members

        public Dictionary<string, string> CollectNamePhonePairs()
        {
            var addressBook = new Dictionary<string, string>(512);

            var service = new ContactsService("Contact Information");
            service.setUserCredentials(_username, _password);

            var query = new ContactsQuery(ContactsQuery.CreateContactsUri("default")) {NumberToRetrieve = 5000};

            ContactsFeed feed = service.Query(query);

            foreach (ContactEntry entry in feed.Entries)
            {
                foreach (PhoneNumber number in entry.Phonenumbers)
                {
                    string ns = number.Value.Replace(" ", "").Replace("+", "");

                    if (ns.StartsWith(_land))
                    {
                        ns = "0" + ns.Remove(0, _land.Length);
                    }

                    if (!addressBook.ContainsKey(ns))
                    {
                        addressBook.Add(ns, entry.Title.Text);
                    }
                }
            }
            return addressBook;
        }

        #endregion
    }
}