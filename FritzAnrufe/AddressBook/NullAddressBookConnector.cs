using System.Collections.Generic;

namespace FritzAnrufe.AddressBook
{
    public class NullAddressBookConnector : IAddressBookConnector
    {
        private static readonly Dictionary<string, string> NullDictionary = new Dictionary<string, string>();

        public Dictionary<string, string> CollectNamePhonePairs()
        {
            return NullDictionary;
        }
    }
}