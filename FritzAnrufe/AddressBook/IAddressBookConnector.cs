using System.Collections.Generic;

namespace FritzAnrufe.AddressBook
{
    internal interface IAddressBookConnector
    {
        Dictionary<string, string> CollectNamePhonePairs();
    }
}