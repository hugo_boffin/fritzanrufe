using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Outlook;

namespace FritzAnrufe.AddressBook
{
    internal class OutlookAddressBookConnector : IAddressBookConnector
    {
        #region IAddressBookConnector Members

        public Dictionary<string, string> CollectNamePhonePairs()
        {
            var addressBook = new Dictionary<string, string>(512);

            var outlookApplication = new Application();
            NameSpace mapiNamespace = outlookApplication.GetNamespace("MAPI");

            MAPIFolder contacts = mapiNamespace.GetDefaultFolder(OlDefaultFolders.olFolderContacts);
            // Specific Contacts Folder: 
            // var contacts = GetFolderByPath(mapiNamespace, @"hello.world@live.de\Kontakte");

            foreach (ContactItem contact in contacts.Items.OfType<ContactItem>())
            {
                if (contact.HomeTelephoneNumber != null)
                {
                    addressBook.Add(contact.FullName + "(Privat)", contact.HomeTelephoneNumber);
                }
                if (contact.BusinessTelephoneNumber != null)
                {
                    addressBook.Add(contact.FullName + "(Gesch�ftlich)", contact.BusinessTelephoneNumber);
                }
                if (contact.MobileTelephoneNumber != null)
                {
                    addressBook.Add(contact.FullName + "(Mobil)", contact.MobileTelephoneNumber);
                }
                if (contact.PrimaryTelephoneNumber != null)
                {
                    addressBook.Add(contact.FullName + "(Prim�r)", contact.PrimaryTelephoneNumber);
                }
            }

            return addressBook;
        }

        #endregion

        private MAPIFolder GetFolderByPath(NameSpace mapiNamespace, string completeFolder)
        {
            string[] folders = completeFolder.Split('\\');

            return folders.Aggregate<string, MAPIFolder>(null,
                (current, folder) =>
                    folder == folders[0]
                        ? mapiNamespace.Folders[folder]
                        : current.Folders[folder]);
        }
    }
}