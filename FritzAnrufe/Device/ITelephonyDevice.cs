using System.Collections.Generic;

namespace FritzAnrufe.Device
{
    internal interface ITelephonyDevice
    {
        List<Call> GetCallList();
        string ClearCallList();
    }
}