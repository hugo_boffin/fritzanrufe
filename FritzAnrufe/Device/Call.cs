using System;

namespace FritzAnrufe.Device
{
    internal struct Call
    {
        public string Called;
        public string Caller;
        public string Date;
        public string Device;
        public string Duration;
        public int Type;

        public Call(string type, string called, string caller, string device, string date, string duration)
        {
            Type = Convert.ToInt32(type) - 1;
            Called = called;
            Caller = caller;
            Device = device;
            Date = date;
            Duration = duration;
        }
    }
}