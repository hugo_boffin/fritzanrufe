using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using FritzAnrufe.Properties;

namespace FritzAnrufe.Device
{
    public class FritzHttpConnector
    {
        public static int BufferSize = 4096;
        public static string address = $"http://{Settings.Default.FritzURL}/cgi-bin/webcm";

        public string Challenge;

        public bool NewAuth;
        public string Response;
        public string SID;
        public bool iswriteaccess;

        public string GetMD5Hash(string md5text)
        {
            if (string.IsNullOrEmpty(md5text))
                return string.Empty;

            var sb = new StringBuilder();

            MD5 md5 = new MD5CryptoServiceProvider();
            foreach (char t in md5text)
            {
                sb.Append(Convert.ToInt16(t) > 255 ? '.' : t);
            }
            byte[] textToHash = Encoding.Unicode.GetBytes(sb.ToString());
            byte[] result = md5.ComputeHash(textToHash);

            sb = new StringBuilder();

            foreach (byte t in result)
                sb.Append(t.ToString("x2"));

            return sb.ToString();
        }

        public string CreateResponse(string Challenge, string Password)
        {
            string response = Challenge + "-" + GetMD5Hash(Challenge + "-" + Password);

            return response;
        }

        public void Login(string Password)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(new StringReader(GetRequest("getpage=../html/login_sid.xml")));

            iswriteaccess = xmlDoc.GetElementsByTagName("iswriteaccess")[0].InnerText.Equals("1");
            SID = xmlDoc.GetElementsByTagName("SID")[0].InnerText;
            Challenge = xmlDoc.GetElementsByTagName("Challenge")[0].InnerText;

            NewAuth = !iswriteaccess;

            if (iswriteaccess)
            {
                // Ohne Passwort nicht implementiert
            }
            else
            {
                Response = CreateResponse(Challenge, Password);
                SID =
                    GetBetween(
                        PostRequest(null, "login:command/response=" + Response + "&getpage=../html/de/menus/menu2.html"),
                        "<input type=\"hidden\" name=\"sid\" value=\"", "\" id=\"uiPostSid\">");
            }
        }

        public string GetBetween(string src, string a, string b)
        {
            int start = src.IndexOf(a, StringComparison.Ordinal) + a.Length;
            int end = src.IndexOf(b, start, StringComparison.Ordinal);
            return src.Substring(start, end - start);
        }

        public string GetFonCallsCSV()
        {
            // Liste auffrischen
            GetRequest("sid=" + SID +
                       "&getpage=../html/de/menus/menu2.html&var:lang=de&var:pagename=foncalls&var:menu=home");

            //GetRequest("sid=" + SID + "&getpage=../html/de/home/foncallsdaten.xml")
            return GetRequest("sid=" + SID + "&getpage=../html/de/FRITZ!Box_Anrufliste.csv");
        }


        private string GetRequest(string getParams)
        {
            var Answer = new StringBuilder();

            string FullAddress = address;
            if (!string.IsNullOrEmpty("getParams"))
                FullAddress += "?" + getParams;

            var myRequest = (HttpWebRequest) WebRequest.Create(FullAddress);
            myRequest.Method = "GET";

            var myHttpWebResponse = (HttpWebResponse) myRequest.GetResponse();
            Stream streamResponse = myHttpWebResponse.GetResponseStream();
            var streamRead = new StreamReader(streamResponse);

            var readBuffer = new Char[BufferSize];

            int count = streamRead.Read(readBuffer, 0, BufferSize);

            while (count > 0)
            {
                var resultData = new String(readBuffer, 0, count);
                Answer.Append(resultData);
                count = streamRead.Read(readBuffer, 0, BufferSize);
            }

            streamRead.Close();
            streamResponse.Close();
            myHttpWebResponse.Close();

            return Answer.ToString();
        }

        private string PostRequest(string getParams, string postData)
        {
            var Answer = new StringBuilder();

            string FullAddress = address;
            if (!string.IsNullOrEmpty(getParams))
                FullAddress += "?" + getParams;

            Console.WriteLine(FullAddress);
            Console.WriteLine(postData);

            var myRequest = (HttpWebRequest) WebRequest.Create(FullAddress);

            myRequest.Method = "POST";

            var encoding = new ASCIIEncoding();
            byte[] buffer = encoding.GetBytes(postData);

            myRequest.ContentType = "application/x-www-form-urlencoded";
            myRequest.ContentLength = buffer.Length;

            Stream newStream = myRequest.GetRequestStream();
            newStream.Write(buffer, 0, buffer.Length);
            newStream.Close();

            var myHttpWebResponse = (HttpWebResponse) myRequest.GetResponse();

            Stream streamResponse = myHttpWebResponse.GetResponseStream();

            var streamRead = new StreamReader(streamResponse);

            var readBuffer = new Char[BufferSize];

            int count = streamRead.Read(readBuffer, 0, BufferSize);

            while (count > 0)
            {
                var resultData = new String(readBuffer, 0, count);
                Answer.Append(resultData);
                count = streamRead.Read(readBuffer, 0, BufferSize);
            }

            streamRead.Close();
            streamResponse.Close();
            myHttpWebResponse.Close();

            return Answer.ToString();
        }

        public void Logout()
        {
            GetRequest("sid=" + SID + "&security:command/logout=1&getpage=../html/de/menus/menu2.html");
        }

        public void ClearJournal()
        {
            PostRequest(null,
                "sid=" + SID +
                "&getpage=../html/de/menus/menu2.html&errorpage=../html/index.html&var:menu=home&var:pagename=home&var:pagetitle=&telcfg:settings/ClearJournal=1");
        }
    }
}