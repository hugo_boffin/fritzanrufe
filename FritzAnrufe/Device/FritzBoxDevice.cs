using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using FritzAnrufe.Properties;

namespace FritzAnrufe.Device
{
    internal class FritzBoxDevice : ITelephonyDevice
    {
        private readonly MainForm _mainForm;

        public FritzBoxDevice(MainForm mainForm)
        {
            _mainForm = mainForm;
        }

        #region ITelephonyDevice Members
        
        public List<Call> GetCallList()
        {
            XmlDocument callListUrlXml = SoapRequest($"http://{Settings.Default.FritzURL}:49000/upnp/control/x_contact",
                "urn:dslforum-org:service:X_AVM-DE_OnTel:1", "GetCallList",
                new CredentialCache
                {
                    {
                        new Uri($"http://{Settings.Default.FritzURL}:49000/upnp/control/x_contact")
                        ,
                        "Digest",
                        new NetworkCredential("dslf-config",
                            Utility.Base64Decode(Settings.Default.FritzPassword))
                    }
                });

            //callListUrlXml.Save(Console.Out);
            //Environment.Exit(0);

            string listUrl = callListUrlXml["s:Envelope"]["s:Body"]["u:GetCallListResponse"]["NewCallListURL"].InnerText;
            string callListXml = PlainHttpRequest(listUrl);

            var doc = new XmlDocument();
            doc.LoadXml(callListXml);

            //doc.Save(Console.Out);

            XmlElement root = doc["root"];


            //TODO: Nicht nur Call Tags, mit xpath abfragen
            return (from XmlNode callNode in root.GetElementsByTagName("Call")
                select
                    new Call(callNode["Type"].InnerText, callNode["Called"].InnerText, callNode["Caller"].InnerText,
                        callNode["Device"].InnerText, callNode["Date"].InnerText, callNode["Duration"].InnerText))
                .ToList();
        }

        public string ClearCallList()
        {
            string str = PlainSoapRequest($"http://{Settings.Default.FritzURL}:49000/upnp/control/x_contact",
                "urn:dslforum-org:service:X_AVM-DE_OnTel:1", "ClearCallList",
                new CredentialCache
                {
                    {
                        new Uri($"http://{Settings.Default.FritzURL}:49000/upnp/control/x_contact"), "Digest"
                        ,
                        new NetworkCredential("dslf-config",
                            Utility.Base64Decode(Settings.Default.FritzPassword))
                    }
                });

            return str;
        }

        #endregion

        private XmlDocument SoapRequest(string url, string serviceType, string function, CredentialCache loginCache)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(PlainSoapRequest(url, serviceType, function, loginCache));
            return xmlDoc;
        }

        private string PlainSoapRequest(string url, string serviceType, string function, CredentialCache loginCache)
        {
            string xmlString =
                $"<?xml version=\"1.0\"?><s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><u:{function} xmlns:u=\"{serviceType}\" /></s:Body></s:Envelope>";

            var request = (HttpWebRequest) WebRequest.Create(url);
            var encoding = new ASCIIEncoding();

            byte[] bytesToWrite = encoding.GetBytes(xmlString);
            request.Credentials = loginCache;
            request.Method = "POST";
            request.ContentLength = bytesToWrite.Length;
            request.Headers.Add(@"SOAPAction: " + serviceType + "#" + function);
            request.ContentType = "text/xml; charset=utf-8";

            Stream newStream = request.GetRequestStream();
            newStream.Write(bytesToWrite, 0, bytesToWrite.Length);
            newStream.Close();

            var response = (HttpWebResponse) request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            return responseFromServer;
        }

        private string PlainHttpRequest(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                // !!HACKHACK simply ignore certificate validation, fast fix for latest firmware changes in FritzOS
            var loginCache = new CredentialCache
            {
                {
                    new Uri(url), "Digest",
                    new NetworkCredential("dslf-config", Utility.Base64Decode(Settings.Default.FritzPassword))
                }
            };

            WebRequest request = WebRequest.Create(url);
            request.Credentials = loginCache;
            request.Method = "GET";
            request.ContentType = "text/xml; charset=utf-8";

            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();

            return responseFromServer;
        }
    }
}