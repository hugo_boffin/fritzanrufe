﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace FritzAnrufe
{
    internal static class Program
    {
        private const string AppGuid = "FRITZ_ANRUFE_MUTEX";

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            using (var mutex = new Mutex(false, @"Global\" + AppGuid))
            {
                if (!mutex.WaitOne(0, false))
                {
                    MessageBox.Show("FRITZ!Anrufe läuft bereits!");
                    return;
                }

                GC.Collect();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                new MainForm();
                Application.Run();
            }
        }
    }
}