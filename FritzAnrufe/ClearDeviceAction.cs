using System.Collections.Generic;
using System.Windows.Forms;
using FritzAnrufe.Device;
using FritzAnrufe.Properties;

namespace FritzAnrufe
{
    internal class ClearDeviceAction : IDeviceAction
    {
        #region IDeviceAction Members

        public List<ListViewItem> Run(MainForm mainForm, ITelephonyDevice telephonyDevice)
        {
            //TODO: Clear Befehl fuer TR-64
            //Console.WriteLine(telephonyDevice.ClearCallList());

            var fritz = new FritzHttpConnector();
            fritz.Login(Utility.Base64Decode(Settings.Default.FritzPassword));
            fritz.ClearJournal();
            fritz.Logout();

            return null;
        }

        #endregion
    }
}